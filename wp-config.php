<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_ncob_site' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '24368' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'TjsReN2!=?_]n}-*DW(:tCI=O(G&F.HaBRWd)|g+~}G=H!Yy0xJA0l!!nr-T3FTD' );
define( 'SECURE_AUTH_KEY',  'X!Z[)dHovNX9ETZ-5qE:@[q9TZk#nJRuO1Jfzo!T^4-2{Z2Zb?BqCAa#jTl4~QI%' );
define( 'LOGGED_IN_KEY',    '~S%&-lwThz&5<XdClL_pPknllAkUUO(TsL/bpkd,W1}GLza#;_26?@*]819,2$`H' );
define( 'NONCE_KEY',        '~K2]~EQcXzDmP?W]SnB.;]q02%3z;z1J1FEM@A >-ky,i3#{&ZDb#.>a;euQLS&)' );
define( 'AUTH_SALT',        'i5vU:ZOHs!Qe~vzmh{:([h/+vwlMSj5c>1&u1nY@vm9!j^Q{+;uB`X% J,jH+nC ' );
define( 'SECURE_AUTH_SALT', 'x.n*@-!]KPt`6$ElipUUjy,p2QHg:IP(2@W+ O=`fulM1pPM Py^!4zjsp9qWajB' );
define( 'LOGGED_IN_SALT',   '/Cf`bw-v>sbJp!Ur1Eyh=`OEB:Fyh=/mc434?^W~uw=E#jnC96tr0zD~Txpo_*a-' );
define( 'NONCE_SALT',       'YCQ^[elWm:ypdUDkq_PASO&,eoOD|>npxaw_(9Q#TX}63[[12gOm{B3|,M$?o<#C' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'nc_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';

define('ALLOW_UNFILTERED_UPLOADS', true);