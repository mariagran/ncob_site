<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NCOB
 */

?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-169429354-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-169429354-1');
	</script>


	<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />

	<link rel="icon" href="img/fav.png" type="image/x-icon" />
	<meta name="theme-color" content="#264162">
	<meta name="apple-mobile-web-app-status-bar-style" content="#264162">
	<meta name="msapplication-navbutton-color" content="#264162">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="keywords" content="css4html, css+for+html, css 4 html, css4, css4 html, css, css3, html, html5" />
	<meta property="og:keywords" content="css4html, css+for+html, css 4 html, css4, css4 html, css, css3, html, html5" />

	<!-- CSS Mobile, resoluções abaixo de 1024px -->
	<!-- <link href="css/estilomobile.css" rel="stylesheet" type="text/css" media="screen and (max-width:1023px)"> -->
	<!-- CSS Computador, resolu??es acima de 1024px -->
	<!-- <link href="css/estilopc.css" rel="stylesheet" type="text/css" media="screen and (min-width:1024px)"> -->
	<!-- Chamar js -->
	<!-- <script src="'https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js'"></script> -->
	<!-- <script language='JavaScript' type='text/JavaScript' src='js/default.js'></script> -->

	<!-- <script src="http://code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script> -->
	<!-- <script type="text/javascript" src="src/jquery.vimeo.api.js"></script> -->

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KZFTGZS"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="all-modal" style="display: none;">
			<div class="box-modal">
				<div class="head-modal">
					<div class="icon-success"></div>
				</div>
				<div class="row">
					<div class="title-modal">
						Mensagem enviada com sucesso!
					</div>
				</div>
				<div class="row">
					<div class="text-modal">
						Em breve entraremos em contato.
					</div>
				</div>
				<div class="row center">
					<button class="bt-close-modal">FECHAR</button>
				</div>
			</div>
		</div>

		<div class="all-loading">
			<div class="logo-load">
			</div>
		</div>

		<div class="header">
			<div class="container">
				<a href="#">
					<div class="logo">
					</div>
				</a>
				<div class="menu-m">
				</div>
				<div class="all-options">
					<div class="area-options">
						<div class="icon-close">
						</div>

						<div class="option" id="go-diferenciais">
							Diferenciais
						</div>
						<div class="option" id="go-como-funciona">
							Como funciona
						</div>
						<div class="option" id="go-contato">
							Contato
						</div>
						<a href="https://ncobapp.freshdesk.com/support/home">
							<button class="bt-option">
								SUPORTE
							</button>
						</a>
					</div>
				</div>
			</div>
		</div>