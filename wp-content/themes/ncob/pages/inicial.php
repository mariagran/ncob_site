<?php

/**
 * Template Name: Inicial
 * Description: Página inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NCOB
 */

get_header(); ?>

<div class="layer-01-01">
</div>

<div class="layer-01-02">
</div>

<div class="area-01-01">

    <div class="align-01-01">
        <div class="row">
            <label class="title-01">INTELIGÊNCIA EM COBRANÇA</label>
        </div>
        <div class="row area-circle">
            <div class="circle-main">
            </div>
            <div class="circle-child" id="go-video-01">
            </div>
        </div>
        <div class="row">
            <label class="text-1">
                Uma empresa dedicada a desenvolver softwares para melhorar
                seu processo de <b style="">gestão de recebíveis.</b>
            </label>
        </div>
    </div>

    <div class="item-decorate-01">
    </div>
</div>
<div class="area-01-02">
    <div class="align-01-01-video">
        <div class="container">
            <div class="area-video-01">


                <iframe id="video-01" src="https://player.vimeo.com/video/428140815" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="item-decorate-01">
    </div>
</div>

<div class="area-02">
    <div class="container op-0">
        <div class="row">
            <label class="title-back">
                DIFERENCIAIS
            </label>
            <label class="title-02-01">
                Quais são os<b> diferenciais?</b>
            </label>
        </div>
        <div class="row itens-funcionalidade">
            <div class="item-02-01">
                <div class="ico-02-01 integracao">
                </div>
                <div class="title-item-02-01">
                    Integração
                </div>
                <div class="text-item-02-01">
                    Acompanhamento de forma centralizada de todos os acionamentos realizados por diferentes fornecedores.
                </div>
            </div>

            <div class="item-02-01">
                <div class="ico-02-01 performance">
                </div>
                <div class="title-item-02-01">
                    Performance
                </div>

                <div class="text-item-02-01">
                    Informações, indicadores e relatórios completo dos resultados e performance de cada fornecedor.
                </div>
            </div>

            <div class="item-02-01">
                <div class="ico-02-01 conhecimento">
                </div>
                <div class="title-item-02-01">
                    Conhecimento
                </div>

                <div class="text-item-02-01">
                    Conheça o comportamento dos clientes e identifique os canais de preferência de cada grupo de cliente.
                </div>
            </div>
            <div class="item-02-01">
                <div class="ico-02-01 segmentacao">
                </div>
                <div class="title-item-02-01">
                    Segmentação
                </div>

                <div class="text-item-02-01">
                    Identifique e distribua seus recebíveis conforme as divisões do seu negócio
                </div>
            </div>
        </div>
    </div>
</div>
<div class="area-03">
    <div class="container">
        <div class="row">
            <label class="title-back">
                O NCOB
            </label>
            <label class="title-02-02">
                O que é o<b> NCOB?</b>
            </label>
        </div>
        <div class="row">
            <div class="box-video-03" id="go-video-02">
                <div class="row area-circle-03">
                    <div class="circle-main-03">
                    </div>
                    <div class="circle-child">
                    </div>
                </div>
            </div>
            <div class="box-video-03-02">
                <iframe id="video-02" src="https://player.vimeo.com/video/307718175" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<div class="area-03">
    <div class="container">
        <div class="row">
            <label class="title-back">
                COMO FUNCIONA
            </label>
            <label class="title-02-02">
                O que faz o<b> NCOB?</b>
            </label>
        </div>
        <div class="row">
            <div class="box-video-03" id="go-video-02">
                <div class="row area-circle-03">
                    <div class="circle-main-03">
                    </div>
                    <div class="circle-child">
                    </div>
                </div>
            </div>
            <div class="box-video-03-02">
                <iframe id="video-02" src="https://player.vimeo.com/video/307717855" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<!-- INÍCIO - FORMULÁRIO -->
<div class="area-contact">
    <div class="container-contact">
        <div class="row">
            <div class="title-back">
                FALE CONOSCO
            </div>
            <div class="title-02-02">
                Formulário de <b> Contato</b>
            </div>
        </div>
        <div class="area-form">
            <div class="area-input col-2-pc">
                <!-- SE OS CAMPOS NÃO FOREM PREENCHIDOS AO CLICAR EM ENVIAR, ADICIONAR CLASSE "error-input" -->
                <div class="title-input">Nome</div>
                <input class="input-default error-input" type="text" autofocus>
            </div>
            <div class="area-input col-2-pc">
                <div class="title-input">Sobrenome</div>
                <input class="input-default error-input" type="text">
            </div>
            <div class="area-input col-1-pc">
                <div class="title-input">E-mail</div>
                <input class="input-default" type="text">
            </div>
            <div class="area-input col-1-pc">
                <div class="title-input">Telefone</div>
                <input class="input-default" type="text">
            </div>
            <div class="area-input col-1-pc">
                <div class="title-input">Assunto</div>
                <select class="select-default">
                    <option>Dúvida</option>
                    <option>Proposta</option>
                    <option>Sugestão</option>
                </select>
            </div>
            <div class="area-input col-1-pc">
                <div class="title-input">Mensagem</div>
                <textarea></textarea>
            </div>
            <div class="row area-bt">
                <!-- SE OS CAMPOS NÃO FOREM PREENCHIDOS AO CLICAR EM ENVIAR, ALTERAR PROPRIEDADE PARA ".show" -->
                <div class="error-text" style="display: none;">Por favor preencha todos os campos!</div>
                <button class="bt-form">
                    Enviar
                </button>
            </div>
        </div>

    </div>
</div>

<?php get_footer();