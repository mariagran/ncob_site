<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NCOB
 */

?>

<!-- FINAL - FORMULÁRIO -->
<div class="area-footer">
	<div class="item-decorate-05">
	</div>
	<div class="container">
		<div class="align-footer">
			<div class="row">
				<div class="box-f">
					<div class="area-logo-footer">
					</div>
				</div>
				<div class="box-f">
					<div class="row m-20">
						<div class="ico-contact email">
						</div>
						<label class="text-contact ">
							contato@ncob.com.br
						</label>
					</div>
					<div class="row">
						<div class="ico-contact phone">
						</div>
						<label class="text-contact">
							(41) 3117-1715
						</label>
					</div>
				</div>
				<div class="box-f">
					<div class="row m-20">
						<div class="ico-contact local m-30">
						</div>
						<label class="text-contact">
							Rua Marechal Deodoro, 857 - CJ 1604 <br>
							Curitiba - PR | CEP: 80060-010
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="area-copy">
	© Copyright 2019. Todos os direitos reservados. NCOB Sistemas e Consultoria LTDA. CNPJ: 23.341.736/0001-29.
</div>
<script type="text/javascript" defer src="//www.123formbuilder.com/embed/5506298.js" data-role="form" data-default-width="650px"></script>
<script type="text/javascript">
	_linkedin_partner_id = "982329";
	window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
	window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script>
<script type="text/javascript">
	(function() {
		var s = document.getElementsByTagName("script")[0];
		var b = document.createElement("script");
		b.type = "text/javascript";
		b.async = true;
		b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
		s.parentNode.insertBefore(b, s);
	})();
</script>
<noscript>
	<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=982329&fmt=gif" />
</noscript>

<?php wp_footer(); ?>

</body>

<script type="text/javascript">
	$(document).ready(function($) {

		$("#go-video-01").click(function() {
			$("#video-01").vimeo("play");
		});

		$('#go-video-02').click(function() {

			$("#video-02").vimeo("play");

		});

	});

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-36251023-1']);
	_gaq.push(['_setDomainName', 'jqueryscript.net']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();
</script>

</html>





<div data-role="page" data-type="page" data-hash="00000006" data-type-id="90001" data-colspan="20" data-num-children="5" data-page-index="0" data-is-first-page="1" data-is-current-page="1" data-is-last-page="yes">
	<div data-role="page-header">
		<div data-role="control" data-type="form-header" data-hash="00000002" data-type-id="27" data-colspan="20" id="form-heading-00000002-acc" aria-labelledby="form-heading-00000002-acc" data-i18n-html="Headline">
			<h1 style="text-align:center;">Formulário de Contato</h1>
			<p>Entre em contato conosco. Deixe-nos saber suas dúvidas, sugestões e preocupações, preenchendo o formulário de contato abaixo.</p>
			<hr>
			<p>&nbsp;</p>
		</div>
	</div>
	<div data-role="page-body">
		<div data-role="container" data-type="virtual-form-table-row" data-hash="00000007" data-type-id="0" data-colspan="20" data-num-children="1">
			<div data-role="control" data-type="name" data-hash="00000008" data-type-id="12" data-colspan="20" data-label-is-bold="1" aria-labelledby="name-00000008-acc name-00000008-error-acc name-00000008-instr-acc" data-is-required="1" data-renderer-type="tln" data-id="65241721">
				<label data-role="label" id="name-00000008-acc" data-i18n-text="control_label_65241721">Nome:</label>
				<dt data-role="instructions" id="name-00000008-instr-acc" data-is-empty="1" data-i18n-text="control_instructions_65241721"></dt>
				<div data-role="input-row" data-is-first-row="1" data-is-last-row="1" data-fill-colspan="10">
					<input data-role="i123-input" type="text" data-index="1" placeholder="First" data-i18n-placeholder="textdef_30" data-size="fill">
					<input data-role="i123-input" type="text" data-index="2" placeholder="Last" data-i18n-placeholder="textdef_31" data-size="half">
				</div>
				<label data-role="error" id="name-00000008-error-acc" data-is-empty="1"></label>
			</div>
		</div>
		<div data-role="container" data-type="virtual-form-table-row" data-hash="00000009" data-type-id="0" data-colspan="20" data-num-children="1">
			<div data-role="control" data-type="email" data-hash="0000000a" data-type-id="5" data-colspan="20" data-label-is-bold="1" data-is-required="1" data-renderer-type="tln" data-id="65241722">
				<label data-role="label" id="email0000000a-acc" data-i18n-text="control_label_65241722">E-mail:</label>
				<dt data-role="instructions" id="email-0000000a-instr-acc" data-is-empty="1" data-i18n-text="control_instructions_65241722"></dt>
				<div data-role="input-row" data-is-first-row="1" data-is-last-row="1" data-fill-colspan="0">
					<input type="email" data-role="i123-input" aria-labelledby="email0000000a-acc email-0000000a-error-acc email-0000000a-instr-acc" data-size="full">
				</div>
				<label data-role="error" id="email-0000000a-error-acc" data-is-empty="1"></label>
			</div>
		</div>
		<div data-role="container" data-type="virtual-form-table-row" data-hash="0000000b" data-type-id="0" data-colspan="20" data-num-children="1">
			<div data-role="control" data-type="phone" data-hash="0000000c" data-type-id="16" data-colspan="20" data-label-is-bold="1" data-renderer-type="tln" data-id="65241723" data-is-active="1">
				<label data-role="label" id="phone-0000000c-acc" data-i18n-text="control_label_65241723">Telefone</label>
				<dt data-role="instructions" id="phone-0000000c-instr-acc" data-is-empty="1" data-i18n-text="control_instructions_65241723"></dt>
				<div data-role="input-row" data-is-first-row="1" data-is-last-row="1" data-fill-colspan="0">
					<input type="text" data-role="i123-input" aria-labelledby="phone-0000000c-acc phone-0000000c-format-acc phone-0000000c-error-acc phone-0000000c-instr-acc" placeholder="## #### #### " data-size="full">
				</div>
				<label data-role="error" id="phone-0000000c-error-acc" data-is-empty="1"></label>
			</div>
		</div>
		<div data-role="container" data-type="virtual-form-table-row" data-hash="0000000d" data-type-id="0" data-colspan="20" data-num-children="1">
			<div data-role="control" data-type="drop-down" data-hash="0000000e" data-type-id="4" data-colspan="20" data-label-is-bold="1" data-is-required="1" data-renderer-type="tln" data-id="65241724">
				<label data-role="label" id="dropdown-0000000e-acc" data-i18n-text="control_label_65241724">Assunto:</label>
				<dt data-role="instructions" id="dropdown-0000000e-instr-acc" data-is-empty="1" data-i18n-text="control_instructions_65241724"></dt>
				<div data-role="input-row" data-is-first-row="1" data-is-last-row="1" data-fill-colspan="0">
					<div data-ui-role="ui-element" data-type="dropdown" data-role="i123-input" aria-labelledby="dropdown-0000000e-acc dropdown-0000000e-error-acc dropdown-0000000e-instr-acc" data-size="full">
						<select>
							<option value="Dúvida" data-index="0">Dúvida</option>
							<option value="Proposta" data-index="1">Proposta</option>
							<option value="Sugestão" data-index="2">Sugestão</option>
						</select>
						<div data-role="dropdown-skin">Dúvida</div>
					</div>
				</div>
				<label data-role="error" id="dropdown-0000000e-error-acc" data-is-empty="1"></label>
			</div>
		</div>
		<div data-role="container" data-type="virtual-form-table-row" data-hash="0000000f" data-type-id="0" data-colspan="20" data-num-children="1">
			<div data-role="control" data-type="textarea" data-hash="00000010" data-type-id="24" data-colspan="20" data-label-is-bold="1" data-is-required="1" data-renderer-type="tln" data-id="65241725">
				<label data-role="label" id="textarea-00000010-acc" data-i18n-text="control_label_65241725">Mensagem:</label>
				<dt data-role="instructions" id="textarea-00000010-instr-acc" data-is-empty="1" data-i18n-text="control_instructions_65241725"></dt>
				<div data-role="input-row" data-is-first-row="1" data-is-last-row="1" data-fill-colspan="0">
					<textarea data-role="i123-input" aria-labelledby="textarea-00000010-acc textarea-00000010-error-acc textarea-00000010-instr-acc" rows="4" data-size="full"></textarea>
				</div>
				<label data-role="error" id="textarea-00000010-error-acc" data-is-empty="1"></label>
			</div>
		</div>
	</div>
	<div data-role="page-footer"><div data-role="control" data-type="form-action-bar" data-hash="00000003" data-type-id="29" data-colspan="20" data-buttons-position="2">
		<button data-role="submit" type="submit" data-is-bold="">
			<span class="normal-state" data-i18n-text="SendButton">Enviar Mensagem</span>
			<span class="submit-state" data-i18n-text="textdef_132">Please wait...</span>
		</button>
		
		

	</div>
</div>
<div data-role="empty-state-view">
	<div data-role="icon">
		<span class="i123-empty-page"></span>
	</div>
	<div data-role="explanation" data-i18n-text="edt_text_add_fields_explanation">Add fields using buttons on the left window or simply drag and drop them here.</div>
</div>
<div data-role="grid">
	<div data-role="edge" data-edge="top"></div>
	<div data-role="edge" data-edge="left"></div>
	<div data-role="edge" data-edge="right"></div>
	<div data-role="edge" data-edge="bottom"></div>
	<div data-index="1" data-role="col">1</div>
	<div data-index="2" data-role="col">2</div>
	<div data-index="3" data-role="col">3</div>
	<div data-index="4" data-role="col">4</div>
	<div data-index="5" data-role="col">5</div>
	<div data-index="6" data-role="col">6</div>
	<div data-index="7" data-role="col">7</div>
	<div data-index="8" data-role="col">8</div>
	<div data-index="9" data-role="col">9</div>
	<div data-index="10" data-role="col">10</div>
	<div data-index="11" data-role="col">11</div>
	<div data-index="12" data-role="col">12</div>
	<div data-index="13" data-role="col">13</div>
	<div data-index="14" data-role="col">14</div>
	<div data-index="15" data-role="col">15</div>
	<div data-index="16" data-role="col">16</div>
	<div data-index="17" data-role="col">17</div>
	<div data-index="18" data-role="col">18</div>
	<div data-index="19" data-role="col">19</div>
	<div data-index="20" data-role="col">20</div>
</div>
</div>