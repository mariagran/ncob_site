<div class="body-formulario-contato">
	<div class="container-formulario">
		<div class="header-formulario">
			<h1>Formulário de contato</h1>
			<p>Entre em contato conosco. Deixe-nos saber suas dúvidas, sugestões e preocupações, preenchendo o formulário de contato abaixo.</p>
		</div>
		<div class="formulario-contato">
			<?php echo do_shortcode('[contact-form-7 id="10" title="Formulário de contato"]'); ?>
		</div>
	</div>
</div>